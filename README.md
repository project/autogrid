# Content Autogrid

The Content Autogrid module displays all content of a certain type and bundle in a table with automatically generated columns.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/autogrid).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/autogrid).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

No other modules are required


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

There is no configuration


## Maintainers

- Martin Anderson-Clutz - [mandclu](https://www.drupal.org/u/mandclu)
