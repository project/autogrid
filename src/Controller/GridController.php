<?php

namespace Drupal\autogrid\Controller;

/**
 * @file
 * Contains \Drupal\autogrid\Controller\GridController.
 */

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\Entity\ConfigEntityType;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contains the primary entity relationship diagram for this module.
 */
class GridController extends ControllerBase {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The set of fields to be output.
   *
   * @var array
   */
  protected $fields;

  /**
   * An EntityListBuilder to create operation links.
   *
   * @var \Drupal\Core\Entity\EntityListBuilder
   */
  protected $entityListBuilder;

  /**
   * Constructs a new GridController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager utility class.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The EntityFieldManager utility class.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The EntityTypeBundleInfo utility class.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The EntityTypeManager utility class.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The EntityTypeManager utility class.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, ModuleHandlerInterface $module_handler, PagerManagerInterface $pager_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->moduleHandler = $module_handler;
    $this->pagerManager = $pager_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('module_handler'),
      $container->get('pager.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    foreach ($this->fields as $field_name => $field) {
      $header[$field_name] = [
        'data' => $field->getLabel(),
        'field' => $field_name,
        'specifier' => $field_name,
      ];
    }
    $header['ops'] = $this->t('Operations');
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    foreach ($this->fields as $field_name => $field) {
      if ($entity->get($field_name)->isEmpty()) {
        $row[$field_name]['data'] = '';
      }
      else {
        $row[$field_name]['data'] = $entity->get($field_name)->view();
        $row[$field_name]['data']['#label_display'] = 'hidden';
      }
    }
    $row['ops']['data'] = $this->entityListBuilder->buildOperations($entity);
    return $row;
  }

  /**
   * Generate the markup to display for the provided entity type and bundle.
   *
   * @param string $entity_type_id
   *   Machine name of the entity type.
   * @param string $bundle
   *   Name of the bundle whose content will be displayed.
   *
   * @return array
   *   Render array to be displayed.
   */
  public function getTable($entity_type_id, $bundle) {
    $bundle_key = $this->entityTypeManager->getDefinition($entity_type_id)->getKey('bundle');
    if (empty($bundle_key) || !$this->entityTypeManager->hasHandler($entity_type_id, 'list_builder')) {
      \Drupal::messenger()->addWarning('Unsupported Entity Type.');
      return $this->redirect('autogrid.choose_entity_type');
    }

    // @todo make the pager length configurable.
    $pager = 10;

    // Build the header first so we can sort on columns.
    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle);

    // @todo make display of base fields configurable.
    $include = ['body', 'title', 'name'];
    // Include the id, which can have different names.
    $include[] = array_key_first($fields);
    foreach ($fields as $field_name => $field_definition) {
      if (strpos($field_name, 'field_') !== 0 && !in_array($field_name, $include)) {
        unset($fields[$field_name]);
      }
    }
    $this->fields = $fields;

    $header = $this->buildHeader();

    $query = \Drupal::entityQuery($entity_type_id)
      ->tableSort($header)
      ->condition($bundle_key, $bundle)
      ->pager($pager);
    $results = $query->execute();
    if (!$results) {
      return [
        '#type' => 'markup',
        '#markup' => '<p>' . $this->t('No matching results found.') . '</p>',
      ];
    }
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entities = $storage->loadMultiple($results);
    $this->entityListBuilder = $this->entityTypeManager->getListBuilder($entity_type_id);
    $rows = [];
    foreach ($entities as $entity) {
      $rows[] = $this->buildRow($entity);
    }

    // Invalidate cache if content in this bundle or the bundle config changes.
    $tags = [
      $entity_type_id . '_list:' . $bundle,
      'config:' . $entity_type_id . '_type:' . $bundle,
    ];

    return [
      'results' => [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      ],
      'pager' => [
        '#type' => 'pager',
      ],
      '#cache' => [
        'contexts' => ['languages', 'url.path', 'user'],
        'tags' => $tags,
        'max-age' => Cache::PERMANENT,
      ],
    ];
  }

  /**
   * No parameters provided, so provide a list of content entity types.
   *
   * @return array
   *   The list of links to choose from.
   */
  public function getEntityTypeLinks() {
    $entity_definitions = $this->entityTypeManager->getDefinitions();
    $entities = [];
    foreach ($entity_definitions as $definition_id => $definition) {
      if ($definition instanceof ConfigEntityType || !$this->entityTypeManager->getDefinition($definition_id)->getKey('bundle') || !$this->entityTypeManager->hasHandler($definition_id, 'list_builder')) {
        continue;
      }
      $url = Url::fromRoute('autogrid.choose_bundle', ['entity_type_id' => $definition_id]);
      // @todo add access check?
      $entities[] = [
        '#type' => 'link',
        '#url' => $url,
        '#title' => $definition->getLabel(),
      ];
    }
    return [
      '#theme' => 'item_list',
      '#items' => $entities,
    ];
  }

  /**
   * No bundle provided, so provide a list of bundles for the entity type.
   *
   * @param string $entity_type_id
   *   The entity whose bundles will be listed.
   *
   * @return array
   *   The list of links to choose from.
   */
  public function getBundleLinks($entity_type_id) {
    $bundle_key = $this->entityTypeManager->getDefinition($entity_type_id)->getKey('bundle');
    if (empty($bundle_key) || !$this->entityTypeManager->hasHandler($entity_type_id, 'list_builder')) {
      \Drupal::messenger()->addWarning('Unsupported Entity Type.');
      return $this->redirect('autogrid.choose_entity_type');
    }
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);
    $links = [];
    foreach ($bundles as $bundle_name => $bundle_info) {
      $url = Url::fromRoute('autogrid.display', ['entity_type_id' => $entity_type_id, 'bundle' => $bundle_name]);
      // @todo add access check?
      $links[] = [
        '#type' => 'link',
        '#url' => $url,
        '#title' => $bundle_info['label'],
      ];
    }
    return [
      '#theme' => 'item_list',
      '#items' => $links,
    ];
  }

  /**
   * Title callback to generate a title based on the parameters.
   *
   * @param string $entity_type_id
   *   Machine name of the entity type.
   * @param string $bundle
   *   Name of the bundle whose content will be displayed.
   *
   * @return string
   *   The title to be used.
   */
  public function getTitle($entity_type_id, $bundle) {
    if (!$entity_type_id || !$bundle) {
      return $this->t('Content Grid');
    }
    $entity_definition = $this->entityTypeManager->getDefinition($entity_type_id);
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
    // @todo more conditional logic to allow for missing/invalid parameters.
    return $this->t('All @type @content', ['@type' => $bundles[$bundle]['label'], '@content' => $entity_definition->getPluralLabel()]);
  }

  /**
   * Returns pager array.
   */
  public function pagerArray($items, $itemsPerPage) {
    // Get total items count.
    $total = count($items);
    // Get the number of the current page.
    $currentPage = $this->pagerManager->createPager($total, $itemsPerPage)->getCurrentPage();
    // Split an array into chunks.
    $chunks = array_chunk($items, $itemsPerPage);
    // Return current group item.
    $currentPageItems = $chunks[$currentPage];
    return $currentPageItems;
  }

}
